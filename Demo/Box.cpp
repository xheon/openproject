
#include "Point.h"
#include "Box.h"

namespace OpenProject
{
	Box::Box( std::shared_ptr<Point> p1,
			 std::shared_ptr<Point> p2,
			 std::shared_ptr<Point> p3,
			 std::shared_ptr<Point> p4,
			 std::shared_ptr<Point> p5,
			 std::shared_ptr<Point> p6,
			 std::shared_ptr<Point> p7,
			 std::shared_ptr<Point> p8 ) :
			  maxForce_(XMVectorSet(5,5,5,0)),
			  maxVelocity_(XMVectorSet(1, 1, 1, 0))
	{
		points_.resize( 8 );

		points_[0] = p1;
		points_[1] = p2;
		points_[2] = p3;
		points_[3] = p4;
		points_[4] = p5;
		points_[5] = p6;
		points_[6] = p7;
		points_[7] = p8;
	}

	void Box::UpdateForce()
	{
		force_ = XMVectorZero();
		for (auto& point : points_)
		{
			force_ += point->force_;
		}

		XMVECTOR comp = XMVectorGreaterOrEqual( force_, maxForce_ );


		if ( (XMVectorGetX( comp ) != 0 || XMVectorGetY( comp ) != 0 || XMVectorGetZ( comp ) != 0 ) && isBroken_ == false)
		{
			isBroken_ = true;
			
			for ( auto& point : points_ )
			{
				point->isBroken = true;
			}
			
			printf( "Broken!!!!\n" );
		}
	}

	bool Box::CheckVelocity()
	{
		XMVECTOR comp = XMVectorGreaterOrEqual(velocity_, maxVelocity_);

		if ((XMVectorGetX(comp) != 0 || XMVectorGetY(comp) != 0 || XMVectorGetZ(comp) != 0) && isBroken_ == false)
		{
			isBroken_ = true;

			for (auto& point : points_)
			{
				point->isBroken = true;
			}

			printf("Broken!!!!\n");
			return true;
		}
		else {
			return false;
		}
	}
}