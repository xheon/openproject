#include <DirectXMath.h>
#include "GeometricPrimitive.h"
#include "Object.h"

using namespace DirectX;

namespace OpenProject
{
	Object::Object(XMVECTOR position, float width, float height, float depth) :
		position_(position),
		height_(height),
		width_(width),
		depth_(depth),
		grid_(4,4,2)
	{
		mass_ = width * height * depth;

		XMVECTOR tensor = (mass_ / 12) * XMVectorSet(
			height * height + depth * depth,
			width * width + depth * depth,
			width * width + height * height, 0);

		initialTensor_ = XMMatrixInverse(nullptr, XMMatrixScalingFromVector(tensor));

		XMVECTOR xcm = XMVectorZero();

		for (auto& point : grid_.points_)
		{
			xcm += point->position_ / (float)grid_.points_.size();
		}

		for (auto& point : grid_.points_)
		{
			point->normalizedPosition_ = point->position_ - xcm;
		}
	}

	std::vector<Object> Object::CreateNewObjects( ) // Add new Objects to the main object vector
	{
		std::vector<Object> newObjects;
		
		XMMATRIX scale = XMMatrixScalingFromVector( XMVectorSet(
			width_	/ (grid_.pointsXCount_ - 1),
			height_ / (grid_.pointsYCount_ - 1),
			depth_	/ (grid_.pointsZCount_ - 1), 0 ) );
		XMMATRIX translate = XMMatrixTranslationFromVector( position_ );
		XMMATRIX rotation = XMMatrixRotationQuaternion( orientation_ );
		XMMATRIX transform = scale * rotation * translate;


		for ( auto& box : grid_.boxes_ )
		{
			if ( box.isBroken_ == true && box.isRemoved_ == false )
			{
				box.isRemoved_ = true;	// Mark the box as deleted		

				float newWidth  = width_ / (grid_.pointsXCount_ - 1);
				float newHeight = height_ / (grid_.pointsYCount_ - 1);
				float newDepth  = depth_ / (grid_.pointsZCount_ - 1);

				if ( newWidth < 0.1f || newHeight < 0.1f || newDepth < 0.1f )
				{
					continue;
				}

				newObjects.emplace_back( 
					XMVector3TransformCoord(box.points_[0]->position_, transform), newWidth, newHeight, newDepth );

				/*printf( "Add new object at %f %f %f\n",
						XMVectorGetX( box.points_[0]->position_),
						XMVectorGetY( box.points_[0]->position_ ),
						XMVectorGetZ( box.points_[0]->position_ ));*/

				mass_ -= (width_ / (grid_.pointsXCount_ - 1)) 
					* (height_ / (grid_.pointsYCount_ - 1))
					* (depth_ / (grid_.pointsZCount_ - 1));

				newObjects.back().grid_.points_[0]->force_ = box.points_[0]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ - 1]->force_ = box.points_[1]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ * (newObjects.back().grid_.pointsYCount_ - 1)]->force_ = box.points_[2]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ * newObjects.back().grid_.pointsYCount_ - 1]->force_ = box.points_[3]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ * newObjects.back().grid_.pointsYCount_ * (newObjects.back().grid_.pointsZCount_ - 1)]->force_ = box.points_[4]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ * newObjects.back().grid_.pointsYCount_ * (newObjects.back().grid_.pointsZCount_ - 1) + newObjects.back().grid_.pointsXCount_ - 1]->force_ = box.points_[5]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.pointsXCount_ * newObjects.back().grid_.pointsYCount_ * (newObjects.back().grid_.pointsZCount_ - 1) + newObjects.back().grid_.pointsXCount_ * (newObjects.back().grid_.pointsYCount_ - 1)]->force_ = box.points_[6]->force_;
				newObjects.back().grid_.points_[newObjects.back().grid_.points_.size() - 1]->force_ = box.points_[7]->force_;
			
				newObjects.back().velocity_ = 50 * box.velocity_;
				//newObjects.back().angularMomentum_ = 0.01f * box.angularMomentum_;
			}
		}

		return newObjects;
	}

	std::vector<std::shared_ptr<Point>> Object::GetCornerPoints()
	{
		std::vector<std::shared_ptr<Point>> cornerPoints(12);

		cornerPoints.push_back(grid_.points_[0]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ * grid_.pointsYCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ * grid_.pointsYCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ * grid_.pointsYCount_ * grid_.pointsZCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ * grid_.pointsYCount_ * grid_.pointsZCount_ - grid_.pointsXCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.pointsXCount_ * grid_.pointsYCount_ * grid_.pointsZCount_ - grid_.pointsXCount_ * grid_.pointsYCount_ - 1]);
		cornerPoints.push_back(grid_.points_[grid_.points_.size() - 1]);

		return cornerPoints;
	}

	void Object::Update(float fElapsedTime)
	{
		// Clear force
		force_ = XMVectorZero(); // XMVectorSet( 0, -9.81f, 0, 0 );
		torque_ = XMVectorSet(0,0,0,0);
		
		for (auto& pt : grid_.points_)
		{
			force_ += pt->force_;
			torque_ += XMVector3Cross(pt->normalizedPosition_, pt->force_);
		}

		for ( auto& box : grid_.boxes_ )
		{
			box.UpdateForce();
		}

		for (auto& pt : grid_.points_)
		{
			pt->force_ = XMVectorZero();
		}

		force_ = force_ - velocity_ * 0.5f;
		torque_ = torque_ - angularMomentum_ * 0.9f;

		velocity_ = velocity_ + fElapsedTime * force_ / mass_;
		position_ = position_ + fElapsedTime * velocity_;

		if ( XMVectorGetY( position_ ) < -0.5f )
		{
			position_ = XMVectorSetY( position_, -0.5f );
		}

		angularMomentum_ = angularMomentum_ + fElapsedTime * torque_;
		inertiaTensor_ = XMMatrixRotationQuaternion(orientation_) * initialTensor_ * XMMatrixTranspose(XMMatrixRotationQuaternion(orientation_));
		angularVelocity_ = XMVector4Transform(angularMomentum_, inertiaTensor_);

		orientation_ = orientation_ + (fElapsedTime / 2) * XMQuaternionMultiply(angularVelocity_, orientation_);
		orientation_ = XMQuaternionNormalize(orientation_);
	}

}