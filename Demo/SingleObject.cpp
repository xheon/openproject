#include "SingleObject.h"

namespace OpenProject
{
	SingleObject::SingleObject( const float width, const float height, const float depth,
		GeometricPrimitive* pGeoObject, const bool isFixed) 
	{
		isFixed_ = isFixed;
		fWidth_ = width;
		fHeight_ = height;
		fDepth_ = depth;

		CreatePoints();

		pGeoObject_ = pGeoObject;
				
		// Pre-compute mass
		float fMass = 0.0f;
		
		for ( auto& it : points_ )
		{
			fMass += it.fMass_;
		}

		fMassInverse_ = 1 / fMass;
		

		// Pre-compute center of mass
		position_ = XMVectorSet( 0, 0, 0, 0 );
		for ( auto& it : points_ )
		{
			position_ += (it.localPosition_ * it.fMass_) * fMassInverse_;
		}

		for ( auto& it : points_ )
		{
			it.localPosition_ = it.localPosition_ - position_;
		}

		// Inertia tensor box
		

		XMVECTOR inertiaTensor = 1 / (12* fMassInverse_)  * XMVectorSet(
			fHeight_ * fHeight_ + fDepth_ * fDepth_,
			fWidth_ * fWidth_ + fDepth_ * fDepth_,
			fWidth_ * fWidth_ + fHeight_ * fHeight_, 0);

		initialTensorInverse_ = XMMatrixInverse(nullptr, XMMatrixScalingFromVector(inertiaTensor));
	
		if (isFixed_) {
			fMassInverse_ = 0.0f;
			initialTensorInverse_ = XMMatrixScalingFromVector(XMVectorZero());
		}
	}

	SingleObject::~SingleObject()
	{}

	void SingleObject::Initialize( XMVECTOR positionCM, XMVECTOR velocityCM, XMVECTOR orientation, XMVECTOR torqueAccumulator )
	{

		position_ = positionCM;
		velocity_ = velocityCM;
		velocityOld_ = velocityCM;
		orientation_ = orientation;
		torqueAccumulator_ = torqueAccumulator;
		torqueAccumulatorOld_ = torqueAccumulator;

		XMQuaternionNormalize( orientation_ );
		XMMATRIX rotation = XMMatrixRotationQuaternion( orientation_ );

		inertiaTensorInverse_ = rotation * initialTensorInverse_ * XMMatrixTranspose(rotation);
		
		angularVelocity_ = XMVector4Transform( torqueAccumulator_, inertiaTensorInverse_ );
	
		for (auto& it : points_) {
			it.worldPosition_ = position_ + XMVector4Transform(it.localPosition_, rotation);
		}
	}

	void SingleObject::Initialize( XMVECTOR positionCM, XMVECTOR velocityCM, XMMATRIX orientation, XMVECTOR torqueAccumulator )
	{
		Initialize( positionCM, velocityCM, XMQuaternionRotationMatrix( orientation ), torqueAccumulator );
	}

	void SingleObject::UpdateForce(XMVECTOR& force, XMVECTOR& torque)
	{
		force = XMVectorSet( 0.0f, 0.0f, 0.0f, 0.0f );
		torque = XMVectorSet( 0.0f, 0.0f, 0.0f, 0.0f );

		for ( auto it = points_.begin(); it != points_.end(); ++it )
		{
			force = XMVectorAdd( force, it->force_ );
			torque = XMVectorAdd( torque, XMVector3Cross( it->localPosition_, it->force_ ) );
		}
	}

	void SingleObject::Update( float fElapsedTime )
	{
		if (isFixed_) return;
		
		// External forces
		XMVECTOR force, torque;

		UpdateForce( force, torque );

		position_ = position_ + fElapsedTime * velocity_;
		velocity_ = velocity_ + fElapsedTime * force * fMassInverse_;
		velocityOld_ = velocity_;

		//orientation_ = orientation_ + fElapsedTime/2 * XMQuaternionMultiply(angularVelocity_, orientation_);
		//orientation_ = XMQuaternionNormalize( orientation_ );

		//torqueAccumulator_ = torqueAccumulator_ + fElapsedTime * torque;
		//torqueAccumulatorOld_ = torqueAccumulator_;


		//XMMATRIX rotation = XMMatrixRotationQuaternion( orientation_ );
		//inertiaTensorInverse_ = rotation * initialTensorInverse_ * XMMatrixTranspose( rotation );
		//angularVelocity_ = XMVector4Transform( torqueAccumulator_, inertiaTensorInverse_ );

		/*for ( auto& it : points_ )
		{
			it.worldPosition_ = position_ + XMVector4Transform( it.localPosition_, rotation );
			it.velocity_ = velocity_ + XMVector3Cross( angularVelocity_, it.localPosition_ );
		}*/
	}


	void SingleObject::CreatePoints()
	{
		points_.clear();

		// Creating points for a box
		points_.push_back( Point{XMVECTOR(), XMVectorSet( 0,			0,			0,		 0 ), XMVECTOR(), XMVECTOR(), 1.0f});
		points_.push_back( Point{XMVECTOR(), XMVectorSet( fWidth_,		0,			0,		 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( fWidth_,		0,			fDepth_, 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( 0,			0,			fDepth_, 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( 0,			fHeight_,	0,		 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( fWidth_,		fHeight_,	0,		 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( fWidth_,		fHeight_,	fDepth_, 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );
		points_.push_back( Point{XMVECTOR(), XMVectorSet( 0,			fHeight_,	fDepth_, 0 ), XMVECTOR(), XMVECTOR(), 1.0f} );

	}

	void SingleObject::SetExternalForce( XMVECTOR force )
	{
		for (int i = 0; i < points_.size(); i++)
		{
			SetExternalForce(force, i);
		}
	}

	void SingleObject::SetExternalForce( XMVECTOR force, int index )
	{
		if (index > points_.size()) return;

		points_[index].force_ = force;

		//printf( "Force: %f %f %f\n", XMVectorGetX( points_[index].force_ ), XMVectorGetY( points_[index].force_ ), XMVectorGetZ( points_[index].force_ ) );
	}


	void SingleObject::AddExternalForce( XMVECTOR force )
	{
		for (int i = 0; i < points_.size(); i++)
		{
			AddExternalForce(force, i);
		}
	}

	void SingleObject::AddExternalForce( XMVECTOR force, int index )
	{
		if (index > points_.size()) return;

		points_[index].force_ += force;

	}


	void SingleObject::SetMass( float fMass )
	{
		float massPoint = fMass / points_.size();

		for (int i = 0; i < points_.size(); i++)
		{
			SetMass(massPoint, i);
		}
	}

	void SingleObject::SetMass( float fMass, int index )
	{
		if (index > points_.size()) return;

		points_[index].fMass_ = fMass;
	}
}