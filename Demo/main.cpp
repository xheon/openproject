//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>
#include <math.h>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

// Own includes
//#include "SingleObject.h"
//#include "ConstrainedObject.h"
#include "Object.h"
#include "Point.h"
#include "collisionDetect.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()).
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor = nullptr;

BasicEffect*                               g_pEffectPositionNormal = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal = nullptr;

BasicEffect*                               g_pEffectPositionNormalColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2( 0, 0 );
XMVECTOR g_vfMovableObjectPos = XMVectorSet( 0, 0, 0, 0 );

// TweakAntBar GUI variables
float g_fGravity = -9.81f;

// Other global variables
float g_fDeltaTime = 1.0f / 128.0f;
float g_fTimeAcc = 0.0f;
bool g_bNextStep = false;
int g_ScreenWidth;
int g_ScreenHeight;

float g_fX, g_fY, g_fZ;

XMVECTOR g_position = XMVectorSet( 0, 5, 0, 0 );
XMVECTOR g_rayStart;
XMVECTOR g_rayDirection;
XMVECTOR g_collisionPosition1;
XMVECTOR g_collisionPosition2;
XMVECTOR g_collisionPosition3;
XMVECTOR g_collisionPosition4;
XMVECTOR g_midA;
XMVECTOR g_midB;

int g_iSetup = 0;
int g_iOldSetup = 0;
bool g_breakable = false;

std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pBox;

std::vector<OpenProject::Object> g_Objects;



// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

void InitObjects( ID3D11DeviceContext* pd3dImmediateContext );

// Create TweakBar and add required buttons and variables
void InitTweakBar( ID3D11Device* pd3dDevice )
{
	//std::string label = "label='" + std::to_string( g_iTimeStep ) + "'";

	TwInit( TW_DIRECT3D11, pd3dDevice );

	g_pTweakBar = TwNewBar( "TweakBar" );
	TwAddButton( g_pTweakBar, "Reset Rigid bodies", []( void * context )
	{
		InitObjects( (ID3D11DeviceContext*)context );
		printf( "Reset current setup\n" );
	}, DXUTGetD3D11DeviceContext(), "" );
	TwAddVarRW( g_pTweakBar, "Setup", TW_TYPE_INT8, &g_iSetup, "min=0 max=1" );


}


// Create a box
void InitObjects( ID3D11DeviceContext* pd3dImmediateContext )
{
	g_Objects.clear();
	g_Objects.reserve( 1000 );

	switch ( g_iSetup )
	{
		case 0:
		{
			// Setup 1
			g_breakable = false;
			g_Objects.emplace_back( XMVectorSet( -2, -0.5f, -1, 0 ), 4, 4, 1 );
			//g_Objects[0].mass_ = 10000;

			g_Objects.emplace_back( XMVectorSet( -0.25f, 2.5f, -3, 0 ), 0.5f, 0.5f, 0.5f );
			g_Objects[1].velocity_ = XMVectorSet( 0, 0, 20, 0 );
			break;
		}
		case 1:
		{
			
			g_Objects.emplace_back( XMVectorSet( -2, -0.5f, -1, 0 ), 4, 4, 1 );
			//g_Objects[0].mass_ = 10000;

			g_Objects.emplace_back( XMVectorSet( -0.25f, 2.5f, -3, 0 ), 0.5f, 0.5f, 0.5f );
			g_Objects[1].velocity_ = XMVectorSet( 0, 0, 20, 0 );
			g_breakable = true;
			g_Objects[1].isBreakable = false;
			break;
		}
	}
}


void DrawWireframeBox( XMMATRIX transform, XMVECTOR position, float width, float height, float depth, XMVECTOR color, ID3D11DeviceContext* pd3dImmediateContext )
{
	g_pEffectPositionColor->Apply( pd3dImmediateContext );
	pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionColor );

	g_pPrimitiveBatchPositionColor->Begin();

	// X
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position, transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, 0, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, 0, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, 0, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, depth, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, 0, depth, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, depth, 0 ), transform ), color ) );

	// Y
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position, transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, 0, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, 0, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, 0, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, depth, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, 0, depth, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, depth, 0 ), transform ), color ) );

	// Z
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position, transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, 0, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, 0, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, 0, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, 0, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( width, height, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, 0, 0 ), transform ), color ),
		VertexPositionColor( XMVector3TransformCoord( position + XMVectorSet( 0, height, depth, 0 ), transform ), color ) );

	g_pPrimitiveBatchPositionColor->End();
}

void DrawWireframeBox( XMMATRIX transform,
					   OpenProject::Box box,
					   ID3D11DeviceContext* pd3dImmediateContext )
{


	if ( box.isRemoved_ == false )
	{

		// X
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[0]->position_, transform ), box.points_[0]->force_ / box.points_[0]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[1]->position_, transform ), box.points_[1]->force_ / box.points_[1]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[2]->position_, transform ), box.points_[2]->force_ / box.points_[2]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[4]->position_, transform ), box.points_[4]->force_ / box.points_[4]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[3]->position_, transform ), box.points_[3]->force_ / box.points_[3]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[6]->position_, transform ), box.points_[6]->force_ / box.points_[6]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[5]->position_, transform ), box.points_[5]->force_ / box.points_[5]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[7]->position_, transform ), box.points_[7]->force_ / box.points_[7]->maxForce_ * Colors::Red ) );

		// Y
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[0]->position_, transform ), box.points_[0]->force_ / box.points_[0]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[2]->position_, transform ), box.points_[2]->force_ / box.points_[2]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[1]->position_, transform ), box.points_[1]->force_ / box.points_[1]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[4]->position_, transform ), box.points_[4]->force_ / box.points_[4]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[3]->position_, transform ), box.points_[3]->force_ / box.points_[3]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[5]->position_, transform ), box.points_[5]->force_ / box.points_[5]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[6]->position_, transform ), box.points_[6]->force_ / box.points_[6]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[7]->position_, transform ), box.points_[7]->force_ / box.points_[7]->maxForce_ * Colors::Red ) );

		// Z
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[0]->position_, transform ), box.points_[0]->force_ / box.points_[0]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[3]->position_, transform ), box.points_[3]->force_ / box.points_[3]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[1]->position_, transform ), box.points_[1]->force_ / box.points_[1]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[5]->position_, transform ), box.points_[5]->force_ / box.points_[5]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[2]->position_, transform ), box.points_[2]->force_ / box.points_[2]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[6]->position_, transform ), box.points_[6]->force_ / box.points_[6]->maxForce_ * Colors::Red ) );

		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVector3TransformCoord( box.points_[4]->position_, transform ), box.points_[4]->force_ / box.points_[4]->maxForce_ * Colors::Red ),
			VertexPositionColor( XMVector3TransformCoord( box.points_[7]->position_, transform ), box.points_[7]->force_ / box.points_[7]->maxForce_ * Colors::Red ) );



	}

}

void DrawWireframeBox( XMMATRIX transform,
					   OpenProject::Point& pt, OpenProject::Point& pX, OpenProject::Point& pY, OpenProject::Point& pZ,
					   OpenProject::Point& pXY, OpenProject::Point& pXZ, OpenProject::Point& pYZ, OpenProject::Point& pXYZ,
					   ID3D11DeviceContext* pd3dImmediateContext )
{

	g_pEffectPositionColor->Apply( pd3dImmediateContext );
	pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionColor );

	g_pPrimitiveBatchPositionColor->Begin();

	// X
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pt.position_, transform ), pt.force_ / pt.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pX.position_, transform ), pX.force_ / pX.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pY.position_, transform ), pY.force_ / pY.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXY.position_, transform ), pXY.force_ / pXY.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pZ.position_, transform ), pZ.force_ / pZ.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXZ.position_, transform ), pXZ.force_ / pXZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pYZ.position_, transform ), pYZ.force_ / pYZ.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXYZ.position_, transform ), pXYZ.force_ / pXYZ.maxForce_ * Colors::Red ) );

	// Y
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pt.position_, transform ), pt.force_ / pt.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pY.position_, transform ), pY.force_ / pY.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pX.position_, transform ), pX.force_ / pX.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXY.position_, transform ), pXY.force_ / pXY.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pZ.position_, transform ), pZ.force_ / pZ.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pYZ.position_, transform ), pYZ.force_ / pYZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pXZ.position_, transform ), pXZ.force_ / pXZ.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXYZ.position_, transform ), pXYZ.force_ / pXYZ.maxForce_ * Colors::Red ) );

	// Z
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pt.position_, transform ), pt.force_ / pt.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pZ.position_, transform ), pZ.force_ / pZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pX.position_, transform ), pX.force_ / pX.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXZ.position_, transform ), pXZ.force_ / pXZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pY.position_, transform ), pY.force_ / pY.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pYZ.position_, transform ), pYZ.force_ / pYZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor( XMVector3TransformCoord( pXY.position_, transform ), pXY.force_ / pXY.maxForce_ * Colors::Red ),
		VertexPositionColor( XMVector3TransformCoord( pXYZ.position_, transform ), pXYZ.force_ / pXYZ.maxForce_ * Colors::Red ) );

	g_pPrimitiveBatchPositionColor->End();

}

void DrawObjects( ID3D11DeviceContext* pd3dImmediateContext )
{
	g_pEffectPositionNormal->SetEmissiveColor( Colors::Black );
	g_pEffectPositionNormal->SetSpecularColor( 0.4f * Colors::White );
	g_pEffectPositionNormal->SetSpecularPower( 100 );
	g_pEffectPositionNormal->SetDiffuseColor( Colors::Red );


	// Bounding Box
	/*XMMATRIX scale = XMMatrixScalingFromVector(
	XMVectorSet( g_Objects[0].width_ / (g_Objects[0].grid_.pointsXCount_ - 1),
	g_Objects[0].height_ / (g_Objects[0].grid_.pointsYCount_ - 1),
	g_Objects[0].depth_ / (g_Objects[0].grid_.pointsZCount_ - 1), 0 ) );
	for ( auto& box : g_Objects[0].grid_.boxes_ )
	{
	if ( box.isRemoved_ == true )
	{
	continue;
	}

	XMMATRIX translate = XMMatrixTranslationFromVector( g_Objects[0].position_ + XMVector3TransformCoord((box.points_[0]->position_ + box.points_[7]->position_) / 2, scale) );
	XMMATRIX rotation = XMMatrixRotationQuaternion( g_Objects[0].orientation_ );
	XMMATRIX transform = scale * rotation * translate;

	g_pEffectPositionNormal->SetWorld( transform );
	g_pBox->Draw( g_pEffectPositionNormal, g_pInputLayoutPositionNormal );


	}*/

	for ( auto& it : g_Objects )
	{
		XMMATRIX scale = XMMatrixScalingFromVector(
			XMVectorSet( it.width_ / (it.grid_.pointsXCount_ - 1),
			it.height_ / (it.grid_.pointsYCount_ - 1),
			it.depth_ / (it.grid_.pointsZCount_ - 1), 0 ) );
		XMMATRIX translate = XMMatrixTranslationFromVector( it.position_ );
		XMMATRIX rotation = XMMatrixRotationQuaternion( it.orientation_ );
		XMMATRIX transform = scale * rotation * translate;

		g_pEffectPositionNormal->SetWorld( transform );
		//g_pBox->Draw( g_pEffectPositionNormal, g_pInputLayoutPositionNormal );

		g_pEffectPositionColor->Apply( pd3dImmediateContext );
		pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionColor );

		g_pPrimitiveBatchPositionColor->Begin();

		for ( auto box : it.grid_.boxes_ )
		{
			DrawWireframeBox( transform, box, pd3dImmediateContext );
		}

		g_pPrimitiveBatchPositionColor->End();

		translate = XMMatrixTranslationFromVector( it.position_ );

		DrawWireframeBox( rotation * translate, -XMVectorSet( 0.01f, 0.01f, 0.01f, 0 ), 1.01f * it.width_, 1.01f * it.height_, 1.01f * it.depth_, Colors::Gray, pd3dImmediateContext );
	}
}


// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor( ID3D11DeviceContext* pd3dImmediateContext )
{
	// Setup position/normal/color effect
	g_pEffectPositionNormalColor->SetWorld( XMMatrixIdentity() );
	g_pEffectPositionNormalColor->SetEmissiveColor( Colors::Black );
	g_pEffectPositionNormalColor->SetDiffuseColor( 0.8f * Colors::White );
	g_pEffectPositionNormalColor->SetSpecularColor( 0.4f * Colors::White );
	g_pEffectPositionNormalColor->SetSpecularPower( 1000 );

	g_pEffectPositionNormalColor->Apply( pd3dImmediateContext );
	pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionNormalColor );

	// Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
	const float n = 8;
	XMVECTOR normal = XMVectorSet( 0, 1, 0, 0 );
	XMVECTOR planecenter = XMVectorSet( 0, -1, 0, 0 );

	g_pPrimitiveBatchPositionNormalColor->Begin();
	for ( float z = -n; z < n; z++ )
	{
		for ( float x = -n; x < n; x++ )
		{
			// Quad vertex positions
			XMVECTOR pos[] = {XMVectorSet( x, -0.5f, z + 1, 0 ),
				XMVectorSet( x + 1, -0.5f, z + 1, 0 ),
				XMVectorSet( x + 1, -0.5f, z, 0 ),
				XMVectorSet( x, -0.5f, z, 0 )};

			// Color checkerboard pattern (white & gray)
			XMVECTOR color = ((int( z + x ) % 2) == 0) ? XMVectorSet( 1, 1, 1, 1 ) : XMVectorSet( 0.6f, 0.6f, 0.6f, 1 );

			// Color attenuation based on distance to plane center
			float attenuation[] = {
				1.0f - XMVectorGetX( XMVector3Length( pos[0] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[1] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[2] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[3] - planecenter ) ) / n};

			g_pPrimitiveBatchPositionNormalColor->DrawQuad(
				VertexPositionNormalColor( pos[0], normal, attenuation[0] * color ),
				VertexPositionNormalColor( pos[1], normal, attenuation[1] * color ),
				VertexPositionNormalColor( pos[2], normal, attenuation[2] * color ),
				VertexPositionNormalColor( pos[3], normal, attenuation[3] * color )
				);
		}
	}
	g_pPrimitiveBatchPositionNormalColor->End();
}

//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
									   DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}

// ============================================================
// DXUT Callbacks
// ============================================================
//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

	std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;

	// Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if ( FAILED( hr = D3DX11CreateEffectFromFile( effectPath.c_str(), 0, pd3dDevice, &g_pEffect ) ) )
	{
		std::wcout << L"Failed creating effect with error code " << int( hr ) << std::endl;
		return hr;
	}


	g_pSphere = GeometricPrimitive::CreateGeoSphere( pd3dImmediateContext, 0.1f, 5, false );
	g_pBox = GeometricPrimitive::CreateCube( pd3dImmediateContext, 1.0f, false );
	//g_pSphere = GeometricPrimitive::CreateGeoSphere( pd3dImmediateContext, 2.0f, false );

	// Init AntTweakBar GUI
	InitTweakBar( pd3dDevice );

	InitObjects( pd3dImmediateContext );

	// Create DirectXTK geometric primitives for later usage

	// Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionColor = new BasicEffect( pd3dDevice );
		g_pEffectPositionColor->SetVertexColorEnabled( true ); // triggers usage of position/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionColor->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionColor::InputElements,
									   VertexPositionColor::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionColor );

		// Primitive batch
		g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>( pd3dImmediateContext );
	}

	// Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormal = new BasicEffect( pd3dDevice );
		g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
		g_pEffectPositionNormal->SetPerPixelLighting( true );

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormal->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionNormal::InputElements,
									   VertexPositionNormal::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionNormal );

		// Primitive batch
		g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>( pd3dImmediateContext );
	}

	// Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormalColor = new BasicEffect( pd3dDevice );
		g_pEffectPositionNormalColor->SetPerPixelLighting( true );
		g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
		g_pEffectPositionNormalColor->SetVertexColorEnabled( true ); // triggers usage of position/normal/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormalColor->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionNormalColor::InputElements,
									   VertexPositionNormalColor::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionNormalColor );

		// Primitive batch
		g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>( pd3dImmediateContext );
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE( g_pEffect );

	TwDeleteBar( g_pTweakBar );
	g_pTweakBar = nullptr;
	TwTerminate();


	SAFE_DELETE( g_pPrimitiveBatchPositionColor );
	SAFE_RELEASE( g_pInputLayoutPositionColor );
	SAFE_DELETE( g_pEffectPositionColor );

	SAFE_DELETE( g_pPrimitiveBatchPositionNormal );
	SAFE_RELEASE( g_pInputLayoutPositionNormal );
	SAFE_DELETE( g_pEffectPositionNormal );

	SAFE_DELETE( g_pPrimitiveBatchPositionNormalColor );
	SAFE_RELEASE( g_pInputLayoutPositionNormalColor );
	SAFE_DELETE( g_pEffectPositionNormalColor );
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
										  const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	// Update camera parameters
	g_ScreenWidth = pBackBufferSurfaceDesc->Width;
	g_ScreenHeight = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow( g_ScreenWidth, g_ScreenHeight );
	g_camera.SetProjParams( XM_PI / 4.0f, float( g_ScreenWidth ) / float( g_ScreenHeight ), 0.1f, 100.0f );

	// Inform AntTweakBar about back buffer resolution change
	TwWindowSize( pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height );

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
	HRESULT hr;

	if ( bKeyDown )
	{
		switch ( nChar )
		{
			// RETURN: toggle fullscreen
			case VK_RETURN:
			{
				g_Objects[1].velocity_ = XMVectorSet( 0, 0, 20, 0 );
				break;
			}
			// F8: Take screenshot
			case VK_F8:
			{
				// Save current render target as png
				static int nr = 0;
				std::wstringstream ss;
				ss << L"Screenshot" << std::setfill( L'0' ) << std::setw( 4 ) << nr++ << L".png";

				ID3D11Resource* pTex2D = nullptr;
				DXUTGetD3D11RenderTargetView()->GetResource( &pTex2D );
				SaveWICTextureToFile( DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str() );
				SAFE_RELEASE( pTex2D );

				std::wcout << L"Screenshot written to " << ss.str() << std::endl;
				break;
			}
			// F10: Toggle video recording
			case VK_F10:
			{
				if ( !g_pFFmpegVideoRecorder )
				{
					g_pFFmpegVideoRecorder = new FFmpeg( 25, 21, FFmpeg::MODE_INTERPOLATE );
					V( g_pFFmpegVideoRecorder->StartRecording( DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi" ) );
				}
				else
				{
					g_pFFmpegVideoRecorder->StopRecording();
					SAFE_DELETE( g_pFFmpegVideoRecorder );
				}
			}
			// Space next step
			case VK_SPACE:
				g_bNextStep = !g_bNextStep;

		}
	}
}

//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
					   bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
					   int xPos, int yPos, void* pUserContext )
{

	float x = (2.0f * xPos) / g_ScreenWidth - 1;
	float y = 1 - (2.0f * yPos) / g_ScreenHeight;

	XMMATRIX trans = XMMatrixInverse( nullptr, g_camera.GetViewMatrix() * g_camera.GetProjMatrix() );

	g_rayStart = XMVector3TransformCoord( XMVectorSet( x, y, 1, 1 ), trans );
	XMVECTOR rayEnd = XMVector3TransformCoord( XMVectorSet( x, y, 0, 1 ), trans );

	g_rayDirection = XMVector3Normalize( rayEnd - g_rayStart );

	// Track mouse movement if left mouse key is pressed
	{
		static int xPosSave = 0, yPosSave = 0;

		if ( bLeftButtonDown )
		{
			// Accumulate deltas in g_viMouseDelta
			g_viMouseDelta.x += xPos - xPosSave;
			g_viMouseDelta.y += yPos - yPosSave;

		}

		xPosSave = xPos;
		yPosSave = yPos;
	}
}

//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
						  bool* pbNoFurtherProcessing, void* pUserContext )
{
	// Send message to AntTweakbar first
	if ( TwEventWin( hWnd, uMsg, wParam, lParam ) )
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	// If message not processed yet, send to camera
	if ( g_camera.HandleMessages( hWnd, uMsg, wParam, lParam ) )
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	return 0;
}

void Impulse( OpenProject::Object& a, OpenProject::Object& b, CollisionInfo info, int index )
{

	XMVECTOR vA, vB, xA, xB;
	XMVECTOR vRel;

	// Arbitrary collision point
	if ( !info.isEdge && !info.isFace )
	{
		vA = a.velocity_ + XMVector3Cross( a.angularVelocity_, info.collisionPointWorld[0] );
		vB = b.velocity_ + XMVector3Cross( b.angularVelocity_, info.collisionPointWorld[0] );

		xA = info.collisionPointWorld[0] - a.position_;
		xB = info.collisionPointWorld[0] - b.position_;

		vRel = vA - vB;
	}
	// Cube colliding with a whole face
	else if ( info.isFace )
	{
		XMVECTOR midPoint = XMVectorZero();
		for ( int i = 0; i < 4; ++i )
		{
			midPoint += info.collisionPointWorld[i];
		}
		midPoint /= 4;

		XMVECTOR n = XMVector3Cross(
			XMVectorSubtract( info.collisionPointWorld[1], info.collisionPointWorld[0] ),
			XMVectorSubtract( info.collisionPointWorld[2], info.collisionPointWorld[0] ) );
		info.normalWorld = n;

		vA = a.velocity_ + XMVector3Cross( a.angularVelocity_, midPoint );
		vB = b.velocity_ + XMVector3Cross( b.angularVelocity_, midPoint );

		xA = midPoint - a.position_;
		xB = midPoint - b.position_;

		vRel = vA - vB;


	}
	// Cube colliding with edge
	else
	{
		XMVECTOR midPoint = XMVectorZero();
		for ( int i = 0; i < 2; ++i )
		{
			midPoint += info.collisionPointWorld[i];
		}
		midPoint /= 2;

		vA = a.velocity_ + XMVector3Cross( a.angularVelocity_, midPoint );
		vB = b.velocity_ + XMVector3Cross( b.angularVelocity_, midPoint );

		xA = midPoint - a.position_;
		xB = midPoint - b.position_;

		vRel = vA - vB;
	}

	if ( XMVectorGetX( (XMVector3Dot( vRel, info.normalWorld )) ) < 0 )
	{
		float j = XMVectorGetX( (XMVector3Dot( -(1 + 1.0f) * vRel, info.normalWorld )) ) /
			(1 / a.mass_ + 1 / b.mass_ +
			XMVectorGetX( XMVector3Dot( (
			XMVector3Cross(
			XMVector4Transform( XMVector3Cross(
			xA, info.normalWorld ), a.inertiaTensor_ ), xA )
			+
			XMVector3Cross(
			XMVector4Transform( XMVector3Cross(
			xB, info.normalWorld ), b.inertiaTensor_ ), xB )
			), info.normalWorld ) ));

		a.grid_.boxes_[index].velocity_ = (a.velocity_ + j * info.normalWorld * 1 / a.mass_);
		b.velocity_ = -1 * (b.velocity_ - j * info.normalWorld * 1 / b.mass_);

		a.grid_.boxes_[index].angularMomentum_ = (a.angularMomentum_ + XMVector3Cross(
			xA, j * info.normalWorld ));

		b.angularMomentum_ = (b.angularMomentum_ - XMVector3Cross(
			xB, j * info.normalWorld ));

	}
}

//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double dTime, float fElapsedTime, void* pUserContext )
{
	std::wstring title = g_bNextStep ? L"RUNNING" : L"PAUSED";

	UpdateWindowTitle( L"Advanced Collision Simulation -" + title + L"- " );

	// Move camera
	g_camera.FrameMove( fElapsedTime );

	g_fTimeAcc += fElapsedTime;

	while ( g_fTimeAcc > g_fDeltaTime )
	{
		if ( g_iOldSetup != g_iSetup )
		{
			g_iOldSetup = g_iSetup;
			InitObjects( DXUTGetD3D11DeviceContext() );
			TwTerminate();
			InitTweakBar( DXUTGetD3D11Device() );
		}

		if ( g_bNextStep == true )
		{

			// Update all objects
			for ( auto& obj : g_Objects )
			{
				obj.Update( fElapsedTime );
			}

			// Collision
			for ( auto objA = g_Objects.begin(); objA != g_Objects.end() - 1; ++objA )
			{

				XMMATRIX rotationA = XMMatrixRotationQuaternion( objA->orientation_ );
				XMVECTOR midA = objA->position_ + XMVector3TransformCoord( XMVectorSet( objA->width_ / 2, objA->height_ / 2, objA->depth_ / 2, 0 ), rotationA );
				XMMATRIX translateA = XMMatrixTranslationFromVector( midA );
				XMMATRIX transformA = rotationA * translateA;


				for ( auto objB = objA + 1; objB != g_Objects.end(); ++objB )
				{
					XMMATRIX rotationB = XMMatrixRotationQuaternion( objB->orientation_ );
					XMVECTOR midB = objB->position_ + XMVector3TransformCoord( XMVectorSet( objB->width_ / 2, objB->height_ / 2, objB->depth_ / 2, 0 ), rotationB );
					XMMATRIX translateB = XMMatrixTranslationFromVector( midB );
					XMMATRIX transformB = rotationB * translateB;

					CollisionInfo info = checkCollision( transformA, transformB,
														 objA->width_, objA->height_, objA->depth_,
														 objB->width_, objB->height_, objB->depth_ );

					if ( info.isValid == false )
					{
						info = checkCollision( transformB, transformA,
											   objB->width_, objB->height_, objB->depth_,
											   objA->width_, objA->height_, objA->depth_ );
					}


					// Found bounding 
					if ( info.isValid == true || info.isFace == true || info.isEdge == true )
					{
						//printf( "Collision between %i and %i:", std::distance( g_Objects.begin(), objA ), std::distance( g_Objects.begin(), objB ) );

						XMMATRIX rotInv = XMMatrixRotationQuaternion( objA->orientation_ );
						rotInv = XMMatrixInverse( nullptr, rotInv );
						XMVECTOR colPointLocal = info.collisionPointWorld[0] - objA->position_;
						colPointLocal = XMVector3TransformCoord( colPointLocal, rotInv );

						float boxWidth = objA->width_ / (objA->grid_.pointsXCount_ - 1);
						float boxHeight = objA->height_ / (objA->grid_.pointsYCount_ - 1);
						float boxDepth = objA->depth_ / (objA->grid_.pointsZCount_ - 1);

						int xComp = floor( fabs( XMVectorGetX( colPointLocal ) ) / boxWidth );
						int yComp = floor( fabs( XMVectorGetY( colPointLocal ) ) / boxHeight );
						int zComp = floor( fabs( XMVectorGetZ( colPointLocal ) ) / boxDepth );

						if ( xComp > objA->grid_.pointsXCount_ - 2 )
						{
							xComp--;
						}

						if ( yComp > objA->grid_.pointsYCount_ - 2 )
						{
							yComp--;
						}

						if ( zComp > objA->grid_.pointsZCount_ - 2 )
						{
							zComp--;
						}

						int index = xComp +
							yComp * (objA->grid_.pointsXCount_ - 1) +
							zComp * (objA->grid_.pointsXCount_ - 1) * (objA->grid_.pointsYCount_ - 1);

						if ( index > objA->grid_.boxes_.size() )
						{
							continue;
						}

						if ( objA->grid_.boxes_[index].isRemoved_ == false )
						{

							g_midA = midA;
							g_midB = midB;

							g_collisionPosition1 = info.collisionPointWorld[0];
							g_collisionPosition2 = info.collisionPointWorld[1];
							g_collisionPosition3 = info.collisionPointWorld[2];
							g_collisionPosition4 = info.collisionPointWorld[3];

							//printf( "Collision in box %i\n", index );
							Impulse( *objA, *objB, info, index );

							XMVECTOR compVel;
							if ( XMVectorGetX( XMVectorEqual( objA->velocity_, XMVectorZero() ) ) == 0 )
							{
								compVel = objA->grid_.boxes_[index].velocity_ / objA->velocity_;
							}
							else
							{
								compVel = objA->grid_.boxes_[index].velocity_;
							}

							if ( fabs( XMVectorGetX( compVel ) ) > 0.01f || fabs( XMVectorGetY( compVel ) ) > 0.01f || fabs( XMVectorGetZ( compVel ) ) > 0.01f )
							{
								if ( g_breakable == true && objA->isBreakable == true )
								{
									objA->grid_.boxes_[index].isBroken_ = true;
									objA->isBreakable = false;
								}
							}

							if ( !objA->grid_.boxes_[index].isBroken_ )
							{
								objA->velocity_ = objA->grid_.boxes_[index].velocity_;
								objA->angularMomentum_ = objA->grid_.boxes_[index].angularMomentum_;
								objA->grid_.boxes_[index].velocity_ = XMVectorZero();
								objA->grid_.boxes_[index].angularMomentum_ = XMVectorZero();
							}

							//g_bNextStep = false;
						}
						else
						{
							//printf( "\n" );
						}
					}
				}
			}


			// Check for new elements
			std::vector<OpenProject::Object> temp;
			std::vector<OpenProject::Object> newObjects;
			for ( auto& obj : g_Objects )
			{
				temp = obj.CreateNewObjects();
				newObjects.insert( newObjects.end(), temp.begin(), temp.end() );
			}

			if ( newObjects.size() > 1 )
			{
				//printf( "halt" );
			}

			// Insert new objects
			g_Objects.insert( g_Objects.end(), newObjects.begin(), newObjects.end() );



			//MidpointStep();
		}


		g_fTimeAcc -= g_fDeltaTime;
	}


	// Update effects with new view + proj transformations
	g_pEffectPositionColor->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionColor->SetProjection( g_camera.GetProjMatrix() );

	g_pEffectPositionNormal->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionNormal->SetProjection( g_camera.GetProjMatrix() );

	g_pEffectPositionNormalColor->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionNormalColor->SetProjection( g_camera.GetProjMatrix() );

	/*
	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if ( g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0 )
	{
	// Calcuate camera directions in world space
	XMMATRIX viewInv = XMMatrixInverse( nullptr, g_camera.GetViewMatrix() );
	XMVECTOR camRightWorld = XMVector3TransformNormal( g_XMIdentityR0, viewInv );
	XMVECTOR camUpWorld = XMVector3TransformNormal( g_XMIdentityR1, viewInv );

	// Add accumulated mouse deltas to movable object pos
	XMVECTOR vMovableObjectPos; // = XMLoadFloat3( &g_vfMovableObjectPos );

	float speedScale = 1.0f;

	g_vfMovableObjectPos = XMVectorSet(0, 0, 0, 0);
	g_vfMovableObjectPos = XMVectorAdd(vMovableObjectPos, speedScale * (float)g_viMouseDelta.x * camRightWorld);
	g_vfMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

	// Reset accumulated mouse deltas
	g_viMouseDelta = XMINT2( 0, 0 );
	}*/
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
								  double fTime, float fElapsedTime, void* pUserContext )
{
	HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = {0.0f, 0.0f, 0.0f, 0.0f};

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

	// Draw floor
	DrawFloor( pd3dImmediateContext );

	//DrawWall( pd3dImmediateContext );

	DrawObjects( pd3dImmediateContext );

	//DrawSpring(pd3dImmediateContext);

	//DrawPoints(pd3dImmediateContext);

	// Draw GUI
	TwDraw();

	if ( g_pFFmpegVideoRecorder )
	{
		V( g_pFFmpegVideoRecorder->AddFrame( pd3dImmediateContext, DXUTGetD3D11RenderTargetView() ) );
	}
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

	// Init camera
	XMFLOAT3 eye( 0.0f, 3.0f, -6.0f );
	XMFLOAT3 lookAt( 0.0f, 2.0f, 0.0f );
	g_camera.SetViewParams( XMLoadFloat3( &eye ), XMLoadFloat3( &lookAt ) );
	g_camera.SetButtonMasks( MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON );

	// Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Advanced Collision Simulation" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 800, 600 );

	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

	return DXUTGetExitCode();
}