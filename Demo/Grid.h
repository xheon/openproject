#ifndef OPENPROJECT_GRID_H
#define	OPENPROJECT_GRID_H

#include <vector>
#include <DirectXMath.h>
#include "GeometricPrimitive.h"
#include "Point.h"
#include "Box.h"

using namespace DirectX;

namespace OpenProject
{
	enum E_NEIGHBOR
	{
		X_POS,
		X_NEG,
		Y_POS,
		Y_NEG,
		Z_POS,
		Z_NEG
	};
	
	class Grid
	{
	public:
		std::vector<std::shared_ptr<Point>> points_;
		std::vector<Box> boxes_;
		
		//XMVECTOR position_;
		//XMVECTOR velocity_;

		// Properties of the grid
		//float height_;
		//float width_;
		//float depth_;

		int pointsXCount_;
		int pointsYCount_;
		int pointsZCount_;

		Grid(int pointsX, int pointsY, int pointsZ);

		void Update(float fElapsedTime);
		Point* AddForce(XMVECTOR forcePosition, XMVECTOR force);

		//Point& GetNeighbor( const Point& point, E_NEIGHBOR neighborType );

	};
}


#endif /* OPENPROJECT_GRID_H */