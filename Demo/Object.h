#ifndef OPENPROJECT_OBJECT_H
#define OPENPROJECT_OBJECT_H

#include <vector>
#include <DirectXMath.h>
#include "GeometricPrimitive.h"
#include "Grid.h"

using namespace DirectX;

namespace OpenProject
{
	class Object
	{
	public:
		float mass_;
		float height_;
		float width_;
		float depth_;
		Grid grid_;
		bool isBreakable = true;
		
		// Linear
		XMVECTOR position_ = XMVectorZero();
		XMVECTOR velocity_ = XMVectorZero();
		XMVECTOR force_ = XMVectorZero();
		
		// Rotational
		XMVECTOR torque_ = XMVectorZero();
		XMVECTOR angularVelocity_ = XMVectorZero();
		XMVECTOR orientation_ = XMQuaternionRotationMatrix(XMMatrixIdentity());
		XMVECTOR angularMomentum_ = XMVectorZero();
		XMMATRIX inertiaTensor_ = XMMatrixIdentity();
		XMMATRIX initialTensor_ = XMMatrixIdentity();

		Object( XMVECTOR position, float width, float height, float depth );

		void Update( float fElapsedTime );
		std::vector<Object> Object::CreateNewObjects(); // Add new Objects to the main object vector
		std::vector<std::shared_ptr<Point>> GetCornerPoints();
	};

}


#endif /* OPENPROJECT_OBJECT_H */