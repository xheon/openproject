#ifndef OPENPROJECT_POINT_H
#define	OPENPROJECT_POINT_H

#include <DirectXMath.h>
#include "GeometricPrimitive.h"

using namespace DirectX;


namespace OpenProject
{
	class Point
	{
	public:
		XMVECTOR position_ = XMVectorZero();
		XMVECTOR normalizedPosition_ = XMVectorZero();
		XMVECTOR maxForce_ = XMVectorSet(5,5,5,0);
		XMVECTOR force_ = XMVectorZero();
		bool isBroken = false;

		Point(XMVECTOR position_);
	};
}

#endif /* OPENPROJECT_POINT_H*/