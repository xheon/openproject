#include "ConstrainedObject.h"

namespace OpenProject
{
	ConstrainedObject::ConstrainedObject( SingleObject& objA, SingleObject& objB ) :
		maxForce_( 100 ),
		objectA( objA ),
		objectB( objB )
	{
		position_ = XMVectorSet( 0, 5, 0, 0 );
		mass_ = 2;
	}

	ConstrainedObject::~ConstrainedObject()
	{}
	

	void ConstrainedObject::Update( float fElapsedTime )
	{
		XMVECTOR forceA, forceB;
		XMVECTOR torqueA, torqueB;

		objectA.UpdateForce( forceA, torqueA ); 
		objectB.UpdateForce( forceB, torqueB );

		float forceDifference = fabs(XMVectorGetY( forceA ) - XMVectorGetY( forceB ));

		if ( forceDifference >= maxForce_ )
		{
			isBroken = true;

			printf( "Broken\n" );

			objectA.velocity_ = velocity_;
			objectB.velocity_ = velocity_;

			objectA.position_ = position_ - offset;
			objectB.position_ = position_ + offset;

			return;
		}

		force_ = XMVectorAdd( forceA, forceB );

		velocity_ = velocity_ + fElapsedTime * (force_ / mass_);
		position_ = position_ + fElapsedTime * velocity_;

		objectA.position_ = position_ - offset;
		objectB.position_ = position_ + offset;
	}

	bool ConstrainedObject::AddForce( float force )
	{
		force_ = XMVectorSetY(force_, XMVectorGetY(force_) + force);

		if ( XMVectorGetX(XMVector3Length(force_)) >= maxForce_ )
		{
			isBroken = true;
			return true;
		}

		return false;
	}
}