#ifndef OPENPROJECT_SINGLEOBJECT_H
#define OPENPROJECT_SINGLEOBJECT_H


#include <vector>
#include <DirectXMath.h>
#include "GeometricPrimitive.h"
#include "Object.h"


using namespace DirectX;

namespace OpenProject
{
	struct Point
	{
		XMVECTOR worldPosition_;
		XMVECTOR localPosition_;
		XMVECTOR force_;
		XMVECTOR velocity_;
		float fMass_;
	};

	class SingleObject : Object
	{
	public:
		float fMassInverse_;
		float fWidth_;
		float fHeight_;
		float fDepth_;
		bool isFixed_;
		XMVECTOR position_;
		XMVECTOR velocity_;
		XMVECTOR velocityOld_;
		XMMATRIX inertiaTensorInverse_;
		XMVECTOR orientation_;
		XMVECTOR angularVelocity_;
		XMVECTOR forceAccumulator_;
		XMVECTOR torqueAccumulator_;
		XMVECTOR torqueAccumulatorOld_;
		XMMATRIX transform_;
		XMMATRIX initialTensorInverse_;

		GeometricPrimitive* pGeoObject_;

		std::vector<Point> points_;

		SingleObject( const float width, const float height, const float depth, 
			GeometricPrimitive* pGeoObject, const bool isFixed = false );
		//SingleObject();
		~SingleObject();

		void Initialize( XMVECTOR positionCM, XMVECTOR velocityCM, XMVECTOR orientation, XMVECTOR torqueAccumulator );
		void Initialize( XMVECTOR positionCM, XMVECTOR velocityCM, XMMATRIX orientation, XMVECTOR torqueAccumulator );

		void UpdateForce(XMVECTOR& force, XMVECTOR& torque);
		void Update( float fElapsedTime );

		void CreatePoints();

		void SetExternalForce(XMVECTOR force);
		void SetExternalForce(XMVECTOR force, int index);

		void AddExternalForce(XMVECTOR force);
		void AddExternalForce(XMVECTOR force, int index);

		void SetMass(float fMass);
		void SetMass(float fMass, int index);
		//void SetMassEqual(float fMass);

			
	};
}

#endif /* OPENPROJECT_SINGLEOBJECT_H */