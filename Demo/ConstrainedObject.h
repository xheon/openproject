#ifndef OPENPROJECT_CONSTRAINEDOBJECT_H
#define OPENPROJECT_CONSTRAINEDOBJECT_H

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

#include "GeometricPrimitive.h"

#include "Object.h"
#include "SingleObject.h"

namespace OpenProject
{
	class ConstrainedObject : public Object
	{
	public:
		float maxForce_;

		// Define the current state
		bool isBroken = false;
		SingleObject objectA;
		SingleObject objectB;

		XMVECTOR offset = XMVectorSet( 1.0f, 0, 0, 0 );


		ConstrainedObject(SingleObject& objA, SingleObject& objB);
		~ConstrainedObject();

		void Update( float fElapsedTime );
		bool AddForce( float force );
	};

}

#endif /* OPENPROJECT_CONSTRAINEDOBJECT_H */