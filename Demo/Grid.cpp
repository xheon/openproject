#include <DirectXMath.h>
#include "GeometricPrimitive.h"
#include "Grid.h"

using namespace DirectX;

namespace OpenProject
{
	Grid::Grid(int pointsX, int pointsY, int pointsZ) :
		pointsXCount_(pointsX),
		pointsYCount_(pointsY),
		pointsZCount_(pointsZ)
	{
		// First create the X, then Y and finally the Z points
		for (int z = 0; z < pointsZCount_; ++z)
		{ 
			for (int y = 0; y < pointsYCount_; ++y)
			{
				for (int x = 0; x < pointsXCount_; ++x)
				{
					points_.emplace_back(std::shared_ptr<Point>(new Point(XMVectorSet(x, y, z, 0))));
				}
			}
		}

		// Create boxes
		for ( int k = 0; k < pointsZCount_ - 1; ++k )
		{
			for ( int j = 0; j < pointsYCount_ - 1; ++j )
			{
				for ( int i = 0; i < pointsXCount_ - 1; ++i )
				{
					int index = k * pointsYCount_ * pointsXCount_ + j * pointsXCount_ + i;
					int iXNext = index + 1;
					int iYNext = index + pointsXCount_;
					int iZNext = index + pointsXCount_ * pointsYCount_;
					int iXYNext = iYNext + 1;
					int iXZNext = iZNext + 1;
					int iYZNext = iYNext + pointsXCount_ * pointsYCount_;
					int iXYZNext = iYZNext + 1;



					/*Point& pt = points_[index];
					Point& xNext = points_	[iXNext];
					Point& yNext = points_	[iYNext];
					Point& zNext = points_	[iZNext];
					Point& xyNext = points_	[iXYNext];
					Point& xzNext = points_	[iXZNext];
					Point& yzNext = points_	[iYZNext];
					Point& xyzNext = points_[iXYZNext];*/

					boxes_.emplace_back( 
						 points_[index] , 
						 points_[iXNext] ,
						 points_[iYNext] ,
						 points_[iZNext] ,
						 points_[iXYNext] ,
						 points_[iXZNext] ,
						 points_[iYZNext] ,
						 points_[iXYZNext] );
				}
			}
		}

		//printf( "Boxes created: %i\n", boxes_.size() );
	}



	/*Point& Grid::GetNeighbor( const Point& point, E_NEIGHBOR neighborType )
	{
		switch (neighborType)
		{
			case X_POS:
			{
				return std::find(
			}
	}*/

	// forcePosition is atm in local coord frame
	Point* Grid::AddForce(XMVECTOR forcePosition, XMVECTOR force)
	{
		//XMMATRIX transform = XMMatrixInverse(nullptr, XMMatrixTranslationFromVector())
		
		XMVECTOR distance;
		int index;
		Point* nearestPoint = nullptr;
		float fDistance = -1.0f;
		// Find nearest neighbor

		int i = 0;
		//printf("Find nearest neighbor:\n");
		for (auto& it : points_)
		{
			//if ( it->isBroken == false )
			//{

				float dist = XMVectorGetX( XMVector3LengthSq( XMVectorAbs( forcePosition - it->position_ ) ) );

				if ( dist < fDistance || fDistance == -1.0f )
				{
					fDistance = dist;
					nearestPoint = it.get();
					index = i;
				}

				/*printf( "Point %i: %f %f %f -> %f\n", i,
						XMVectorGetX( it->position_ ),
						XMVectorGetY( it->position_ ),
						XMVectorGetZ( it->position_ ),
						dist );*/

				i++;
			//}
		}

		if (nearestPoint != nullptr)
		{
			
			nearestPoint->force_ += force;

			
			
			/*printf("Force position: %f %f %f\n",
				XMVectorGetX(forcePosition),
				XMVectorGetY(forcePosition),
				XMVectorGetZ(forcePosition));

			printf( "Force: %f %f %f\n",
					XMVectorGetX( force ),
					XMVectorGetY( force ),
					XMVectorGetZ( force ) );


			printf("Point (%i) %f %f %f\n", index,
				XMVectorGetX(nearestPoint->position_),
				XMVectorGetY(nearestPoint->position_),
				XMVectorGetZ(nearestPoint->position_));*/
		}
		else
		{
			//printf("no point found\n");
		}
		return nearestPoint;
	}

}