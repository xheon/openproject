#ifndef OPENPROJECT_BOX_H
#define	OPENPROJECT_BOX_H

#include <vector>
#include "Point.h"

namespace OpenProject
{

	class Box
	{
	public:
		std::vector<std::shared_ptr<Point>> points_;
		XMVECTOR force_ = XMVectorZero();
		XMVECTOR maxForce_;
		bool isBroken_ = false;
		bool isRemoved_ = false;
		XMVECTOR velocity_ = XMVectorZero();
		XMVECTOR maxVelocity_;
		XMVECTOR angularMomentum_ = XMVectorZero();

		Box( std::shared_ptr<Point> p1,
			 std::shared_ptr<Point> p2,
			 std::shared_ptr<Point> p3,
			 std::shared_ptr<Point> p4,
			 std::shared_ptr<Point> p5,
			 std::shared_ptr<Point> p6,
			 std::shared_ptr<Point> p7,
			 std::shared_ptr<Point> p8);
		
		void UpdateForce();
		bool CheckVelocity();
	};
}

#endif /* OPENPROJECT_BOX_H */