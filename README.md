Goal of the open project:
- Simple fracturing of rigid bodies

Approach:
- Top-Down: On force impact smaller objects are created and removed from the original one

Additional:
- Simple bounding volume hierarchy